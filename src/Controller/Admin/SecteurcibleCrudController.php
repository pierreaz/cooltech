<?php

namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Secteurcible;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class SecteurcibleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Secteurcible::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index',"Secteur d'activité ")
            ->setPageTitle('new',"Création d'un secteur d'activité cible")
            ->setPageTitle('edit',"Modification")
            ->setPageTitle('detail',"Détails")
            ->setDefaultSort(['createAt'=>'DESC']);
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
                    ->add(Crud::PAGE_INDEX, Action::DETAIL)
                    //renommage des action
                    ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('NOUVEAU');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('voir');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })

                    ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                        return $action->setIcon('fa fa-reply-all')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('Retour à la liste');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })


                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-primary')
                                      ->setLabel('ENREGISTER & NOUVEAU');
                    })

                    ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
                    // ordre d'alignement des bouttons
                    ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::SAVE_AND_ADD_ANOTHER])
                    ;
    }
    
    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm()->hideOnDetail()->hideOnIndex(),
            TextField::new('nom',"Nom"),
            TextareaField::new('description',"Description"),
            DateTimeField::new('createAt',"Créé")->hideOnForm()->hideOnIndex(),
            DateTimeField::new('updateAt',"Modifié(e)")->hideOnForm()
                                                       ->hideOnIndex(),
        ];
    }
    
    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if (!$entityInstance instanceof Secteurcible){
            return;
        }
        $entityInstance->setCreateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if (!$entityInstance instanceof Secteurcible){
            return;
        }
        $entityInstance->setUpdateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance);
    }
}
