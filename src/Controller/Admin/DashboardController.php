<?php

namespace App\Controller\Admin;

use App\Entity\Activite;
use App\Entity\Categorieelt;
use App\Entity\Cellule;
use App\Entity\Directionsup;
use App\Entity\Direction;
use App\Entity\Division;
use App\Entity\Dossier;
use App\Entity\Dossiers;
use App\Entity\Elementdossi;
use App\Entity\Pays;
use App\Entity\User;
use App\Entity\Personne;
use App\Entity\Pieceidentite;
use App\Entity\Secteurcible;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator
    ) {
    }
    
    //#[Route('/admin', name: 'admin')]
    #[Route('/dashbordhome', name: 'dashbordHome')]
    public function index(): Response
    {
        return $this->render('admin/accueil.html.twig');
        //return parent::index();
        //$url=$this->adminUrlGenerator
       // ->setController(DirectionCrudController::class)
        //->generateUrl();
        //return $this->redirect($url);
        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('HISTORY PROJECT');
    }

    public function configureMenuItems(): iterable
    {
        // yield MenuItem::linkToDashboard('Dashboard', 'fa fa-home');
        // yield MenuItem::linkToCrud('The Label', 'fas fa-list', EntityClass::class);
        yield MenuItem::section('ACCUEIL', 'fa fa-home');
        
    yield MenuItem::section('DOSSIERS');
    yield MenuItem::subMenu('Dossier', 'fas fa-bars')->setSubItems(
        [
            MenuItem::linkToCrud('Ajout dossier', 'fas fa-plus', Dossier::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Liste', 'fas fa-plus', Dossier::class)
        ]
    );

   /* yield MenuItem::subMenu('Dossiers', 'fas fa-bars')->setSubItems(
        [
            MenuItem::linkToCrud('Ajout dossier', 'fas fa-plus', Dossiers::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Liste', 'fas fa-plus', Dossiers::class)
        ]
    );*/
    yield MenuItem::subMenu('Elément', 'fas fa-bars')->setSubItems(
        [
            MenuItem::linkToCrud('Ajout', 'fas fa-plus', Elementdossi::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Liste', 'fas fa-plus', Elementdossi::class)
        ]
    );

    yield MenuItem::subMenu('Catégorie', 'fas fa-bars')->setSubItems(
        [
            MenuItem::linkToCrud('Ajout', 'fas fa-plus', Categorieelt::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Liste', 'fas fa-plus', Categorieelt::class)
        ]
    );
    yield MenuItem::subMenu('Secteur activité cible', 'fas fa-bars')->setSubItems(
        [
            MenuItem::linkToCrud('Ajout activité cible', 'fas fa-plus', Secteurcible::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Liste', 'fas fa-plus', Secteurcible::class)
        ]
    );

    yield MenuItem::section('PARAMETRAGE');
        yield MenuItem::subMenu('Personne', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout personne', 'fas fa-plus', Personne::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Personne::class)
            ]
        );
        yield MenuItem::subMenu('Pays', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout pays', 'fas fa-plus', Pays::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Pays::class)
            ]
        );
        yield MenuItem::subMenu('Profession', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout profession', 'fas fa-plus', Activite::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Activite::class)
            ]
        );
        yield MenuItem::subMenu("Type de pièce d'identité", 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout type', 'fas fa-plus', Pieceidentite::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Pieceidentite::class)
            ]
        );
        
        yield MenuItem::section('ORGANISATION');
        yield MenuItem::subMenu('Direction Général', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout Direction', 'fas fa-plus', Directionsup::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Directionsup::class)
            ]
        );
        yield MenuItem::subMenu('Direction', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout Direction', 'fas fa-plus', Direction::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Direction::class)
            ]
        );
        yield MenuItem::subMenu('Division', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout Division', 'fas fa-plus', Division::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Division::class)
            ]
        );
        yield MenuItem::subMenu('Cellule', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout cellule', 'fas fa-plus', Cellule::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', Cellule::class)
            ]
        );
    // gestion des utilisateurs
        yield MenuItem::section('UTILISATEURS');
        yield MenuItem::subMenu('Utilisateurs', 'fas fa-bars')->setSubItems(
            [
                MenuItem::linkToCrud('Ajout utilisateur', 'fas fa-plus', User::class)->setAction(Crud::PAGE_NEW),
                MenuItem::linkToCrud('Liste', 'fas fa-plus', User::class)
            ]
        );

        yield MenuItem::section('PROFIL');
        yield MenuItem::linkToLogout('Déconnecter', 'fa fa-sign-out');
        //yield MenuItem::linkToRoute('Settings', 'fa fa-user-cog', '...', ['...' => '...']);
        //yield MenuItem::linkToLogout('Profil', 'fa fa-user-cog');
    }
}