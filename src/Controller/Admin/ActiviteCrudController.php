<?php

namespace App\Controller\Admin;

use App\Entity\Activite;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class ActiviteCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Activite::class;
    }
    //
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index','Profession')
            ->setPageTitle('new',"Création d'une nouvelle profession")
            ->setPageTitle('edit',"Modification")
            ->setPageTitle('detail',"Détails")
            ->setDefaultSort(['createAt'=>'DESC']);
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
                    ->add(Crud::PAGE_INDEX, Action::DETAIL)
                    //renommage des action
                    ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('NOUVEAU');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('voir');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })

                    ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                        return $action->setIcon('fa fa-reply-all')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('Retour à la liste');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })


                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-primary')
                                      ->setLabel('ENREGISTER & NOUVEAU');
                    })

                    ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
                    // ordre d'alignement des bouttons
                    ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::SAVE_AND_ADD_ANOTHER])
                    ;
    }
    
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm()
                              ->hideOnIndex()
                              ->hideOnDetail(),
            TextField::new('nom'),
            TextareaField::new('description',"Description"),
            ChoiceField::new('degre',"Degré d'urgence")->setChoices([
                'Non confidentiel' => 'Non confidentiel',
                'Confidentiel' => 'Confidentiel',
            ]),
            BooleanField::new('active'),
            DateTimeField::new('createAt',"Créé")->hideOnForm(),
            DateTimeField::new('updateAt',"Modifié(e)")->hideOnForm()
                                         ->hideOnIndex(),
        ];
        
    }
    
    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if(!$entityInstance instanceof Activite) return;
        $entityInstance->setCreateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance);
    }
    public function updatetEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if(!$entityInstance instanceof Activite) return;
        $entityInstance->setUpdateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance);
    }
}
