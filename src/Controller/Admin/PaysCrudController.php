<?php

namespace App\Controller\Admin;

use App\Entity\Pays;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CountryField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class PaysCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Pays::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index','Pays')
            ->setPageTitle('new',"Création d'un nouveau pays")
            ->setPageTitle('edit',"Modification")
            ->setPageTitle('detail',"Détails")
            ->setDefaultSort(['createAt'=>'DESC']);
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
        ->add(Crud::PAGE_INDEX, Action::DETAIL)
        //renommage des action
        ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
            return $action->setIcon('fa fa-plus')
                          ->setCssClass('btn btn-success')
                          ->setLabel('NOUVEAU');
        })
        ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
            return $action->setIcon('fa fa-plus')
                         // ->setCssClass('btn btn-success')
                          ->setLabel('voir');
        })
        ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
            return $action->setIcon('fa fa-remove')
                          //->setCssClass('btn btn-success')
                          ->setLabel('supprimer');
        })
        ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
            return $action->setIcon('fa fa-pencil')
                          //->setCssClass('btn btn-success')
                          ->setLabel('Modifier');
        })

        ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
            return $action->setIcon('fa fa-reply-all')
                         // ->setCssClass('btn btn-success')
                          ->setLabel('Retour à la liste');
        })
        ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
            return $action->setIcon('fa fa-remove')
                          //->setCssClass('btn btn-success')
                          ->setLabel('supprimer');
        })
        ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
            return $action->setIcon('fa fa-pencil')
                          //->setCssClass('btn btn-success')
                          ->setLabel('Modifier');
        })


        ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
            return $action->setIcon('fa fa fa-floppy-o')
                          ->setCssClass('btn btn-success')
                          ->setLabel('ENREGISTER');
        })
        ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
            return $action->setIcon('fa fa fa-floppy-o')
                          ->setCssClass('btn btn-primary')
                          ->setLabel('ENREGISTER & NOUVEAU');
        })

        ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
            return $action->setIcon('fa fa fa-floppy-o')
                          ->setCssClass('btn btn-success')
                          ->setLabel('ENREGISTER');
        })
        ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
        // ordre d'alignement des bouttons
        ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::SAVE_AND_ADD_ANOTHER])
        ;
    }
    public function configureFields(string $pageName): iterable
    {
    return [
        IdField::new('id')->hideOnForm()->hideOnDetail()->hideOnIndex(),
        CountryField::new('nom',"Nom"),
        TextField::new('indicatif',"Indicatif"),
        TextField ::new('continent',"Continent"),
        NumberField ::new('superficie','Superficie : km2'),
        IntegerField::new('population',"Population"),
        //BooleanField::new('active'),
        DateTimeField::new('createAt',"Créé")->hideOnForm(),
        DateTimeField::new('updateAt',"Modifié(e)")->hideOnForm()
                                                       ->hideOnIndex(),
     ];
 }
    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if (!$entityInstance instanceof Pays){
            return;
        }
        $entityInstance->setCreateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance); 
    }
    public function updateEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if (!$entityInstance instanceof Pays){
            return;
        }
        $entityInstance->setUpdateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance); 
    }
}

