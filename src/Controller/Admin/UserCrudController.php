<?php

namespace App\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use App\Entity\User;
use EasyCorp\Bundle\EasyAdminBundle\Config\{Action, Actions, Crud, KeyValueStore};
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Dto\EntityDto;
use EasyCorp\Bundle\EasyAdminBundle\Field\{IdField, EmailField, TextField};
use Symfony\Component\Form\Extension\Core\Type\{PasswordType, RepeatedType};
use Symfony\Component\Form\{FormBuilderInterface, FormEvent, FormEvents};
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class UserCrudController extends AbstractCrudController
{
    public function __construct(
        public UserPasswordHasherInterface $userPasswordHasher
    ) {}

    public static function getEntityFqcn(): string
    {
        return User::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index','Utilisateurs')
            ->setPageTitle('new',"Nouveau utilisateur")
            ->setPageTitle('edit',"Modification")
            ->setPageTitle('detail',"Détails")
            ->setDefaultSort(['createAt'=>'DESC']);
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
                    ->add(Crud::PAGE_INDEX, Action::DETAIL)
                    //renommage des action
                    ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('NOUVEAU');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('voir');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })

                    ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                        return $action->setIcon('fa fa-reply-all')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('Retour à la liste');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })


                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-primary')
                                      ->setLabel('ENREGISTER & NOUVEAU');
                    })

                    ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
                    // ordre d'alignement des bouttons
                    ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::SAVE_AND_ADD_ANOTHER])
                    ;
    }
    /*public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->add(Crud::PAGE_EDIT, Action::INDEX)
            ->add(Crud::PAGE_INDEX, Action::DETAIL)
            ->add(Crud::PAGE_EDIT, Action::DETAIL)
            ;
    }*/

    public function configureFields(string $pageName): iterable
    {
        $fields = [
            IdField::new('id')->hideOnForm()->hideOnDetail()->hideOnIndex(),
            EmailField::new('email'),
            TextField::new('lastname', "Nom"),
            TextField::new('firstname', "Prénom(s)"),
            BooleanField::new('active', "Active"),
           DateTimeField::new('createAt',"Créé")->hideOnForm(),
           DateTimeField::new('updateAt',"Modifié(e)")->hideOnForm()->hideOnIndex(),
        ];

        $password = TextField::new('password')
            ->setFormType(RepeatedType::class)
            ->setFormTypeOptions([
                'type' => PasswordType::class,
                'first_options' => ['label' =>  "password"],
                'second_options' => ['label' =>  "Répéter le mot de pass"],
                'mapped' => false,
            ])
            ->setRequired($pageName === Crud::PAGE_NEW)
            ->onlyOnForms()
            ;
        $fields[] = $password;

        return $fields;
    }

    public function createNewFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createNewFormBuilder($entityDto, $formOptions, $context);
        return $this->addPasswordEventListener($formBuilder);
    }

    public function createEditFormBuilder(EntityDto $entityDto, KeyValueStore $formOptions, AdminContext $context): FormBuilderInterface
    {
        $formBuilder = parent::createEditFormBuilder($entityDto, $formOptions, $context);
        return $this->addPasswordEventListener($formBuilder);
    }

    private function addPasswordEventListener(FormBuilderInterface $formBuilder): FormBuilderInterface
    {
        return $formBuilder->addEventListener(FormEvents::POST_SUBMIT, $this->hashPassword());
    }

    private function hashPassword() {
        return function($event) {
            $form = $event->getForm();
            if (!$form->isValid()) {
                return;
            }
            $password = $form->get('password')->getData();
            if ($password === null) {
                return;
            }

            $hash = $this->userPasswordHasher->hashPassword($this->getUser(), $password);
            $form->getData()->setPassword($hash);
        };
    }

    public function updateEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if (!$entityInstance instanceof User){
            return;
        }
        $entityInstance->setUpdateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance);
    }
}
