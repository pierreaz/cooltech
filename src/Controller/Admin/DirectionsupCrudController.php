<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use App\Entity\Directionsup;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use phpDocumentor\Reflection\Types\Boolean;

class DirectionsupCrudController extends AbstractCrudController
{
   public const ACTION_DUPLICATE='Duplication';
    public static function getEntityFqcn(): string
    {
        return Directionsup::class;
    }
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index','Direction générale')
            ->setPageTitle('new',"Création d'une nouvelle direction générale")
            ->setPageTitle('edit',"Modification")
            //->setPageTitle('delete',"Suppression")
            ->setPageTitle('detail',"Détails")
            ->setDefaultSort(['createAt'=>'DESC']);
    }
    public function configureFilters(Filters $filters): Filters
    {
        return $filters
            ->add('nom')
            ->add('description')
        ;
}

    public function configureActions(Actions $actions): Actions
    {
        $duplicate=Action::new(self::ACTION_DUPLICATE)
            ->linkToCrudAction('duplicateDirectionsup')
            ->setCssClass('btn btn-info');//définir le css du boutton
       

        return $actions
                    ->add(Crud::PAGE_EDIT, $duplicate)
                    ->add(Crud::PAGE_INDEX, Action::DETAIL)
                    //renommage des action
                    ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                      ->setCssClass('btn btn-success btn-lg')
                                      ->setLabel('NOUVEAU');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('voir');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })

                    ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                        return $action->setIcon('fa fa-reply-all')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('Retour à la liste');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })


                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-primary')
                                      ->setLabel('ENREGISTER & NOUVEAU');
                    })

                    ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE);
                    // ordre d'alignement des bouttons
                    //->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::SAVE_AND_ADD_ANOTHER])
                    //->reorder(Crud::PAGE_EDIT,[self::ACTION_DUPLICATE, Action::SAVE_AND_RETURN]);// définition de l'ordre des buttons 
    }

    //redefinition des attributs
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->hideOnForm()
                             ->hideOnIndex()
                             ->hideOnDetail(),
            TextField::new('nom'),
            TextareaField::new('description',"Description")->hideOnIndex(),
            BooleanField::new('active'),
            DateTimeField::new('createAt',"Créé")->hideOnForm(),
            DateTimeField::new('updateAt',"Modifié(e)")->hideOnForm()
                                                       ->hideOnIndex(),
        ];
    }
    //insertion automatique de la date
    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if(!$entityInstance instanceof Directionsup) return;
        $entityInstance->setCreateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance);
    }

    public function updateEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if(!$entityInstance instanceof Directionsup) return;
        $entityInstance->setUpdateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance);
    }

    //définition de la méthode de duplication
    public function duplicateDirectionsup(
        AdminContext $context,
        AdminUrlGenerator $adminUrlGenerator,
        EntityManagerInterface $em
        ):Response
    {
        /** @var Directionsup $directionsup */
        $directionsup= $context->getEntity()->getInstance();
        $duplicateDirectionsup= clone $directionsup;
        parent::persistEntity($em, $duplicateDirectionsup);
        $url=$adminUrlGenerator
            ->setController(self::class)
            ->setAction(Action::DETAIL)
            ->setEntityId($duplicateDirectionsup->getId())
            ->generateUrl();
        return $this->redirect($url);
    }

    //supprimer une direction supérieur avec toutes les directions associées
   /* public function deleteEntity(EntityManagerInterface $em, $entityInstance): void
    {
        if(!$entityInstance instanceof Directionsup) return;
        foreach($entityInstance->getDirections() as $direction){
            $em->remove($direction);
        }
        parent::deleteEntity($em, $entityInstance);
    }*/
}
