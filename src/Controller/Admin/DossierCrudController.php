<?php

namespace App\Controller\Admin;

use App\Entity\Dossier;
use App\Entity\Personne;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\CollectionField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Translation\TranslatableMessage;

class DossierCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Dossier::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setPageTitle('index','Dossier')
            ->setPageTitle('new',"Création d'un dossier")
            ->setPageTitle('edit',"Modification")
            ->setPageTitle('detail',"Détails");
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
                    ->add(Crud::PAGE_INDEX, Action::DETAIL)
                    //renommage des action
                    ->update(Crud::PAGE_INDEX, Action::NEW, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('NOUVEAU');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DETAIL, function (Action $action) {
                        return $action->setIcon('fa fa-plus')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('voir');
                    })
                    ->update(Crud::PAGE_INDEX, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_INDEX, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })

                    ->update(Crud::PAGE_DETAIL, Action::INDEX, function (Action $action) {
                        return $action->setIcon('fa fa-reply-all')
                                     // ->setCssClass('btn btn-success')
                                      ->setLabel('Retour à la liste');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::DELETE, function (Action $action) {
                        return $action->setIcon('fa fa-remove')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('supprimer');
                    })
                    ->update(Crud::PAGE_DETAIL, Action::EDIT, function (Action $action) {
                        return $action->setIcon('fa fa-pencil')
                                      //->setCssClass('btn btn-success')
                                      ->setLabel('Modifier');
                    })


                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->update(Crud::PAGE_NEW, Action::SAVE_AND_ADD_ANOTHER, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-primary')
                                      ->setLabel('ENREGISTER & NOUVEAU');
                    })

                    ->update(Crud::PAGE_EDIT, Action::SAVE_AND_RETURN, function (Action $action) {
                        return $action->setIcon('fa fa fa-floppy-o')
                                      ->setCssClass('btn btn-success')
                                      ->setLabel('ENREGISTER');
                    })
                    ->remove(Crud::PAGE_EDIT, Action::SAVE_AND_CONTINUE)
                    // ordre d'alignement des bouttons
                    ->reorder(Crud::PAGE_NEW,[Action::SAVE_AND_RETURN,Action::SAVE_AND_ADD_ANOTHER])
                    ;
    }
    public function configureFields(string $pageName): iterable
    {
        $entity= new Dossier;
        return [
            IdField::new('id')->hideOnForm()
                             ->hideOnIndex()
                             ->hideOnDetail(),
            DateTimeField::new('dateouverture', "Date d'ouverture"),
           
        
            TextField::new('nom','Nom'),
            TextField::new('nature', 'Type de dossier')->hideOnIndex(),
            TextField::new('objet', 'Objet'),
             //AssociationField::new('Personne')->setFormTypeOptions(['multiple'=>true]),
            // debut de gestion de personnes suivant les pages
            AssociationField::new('personnes')->onlyOnForms()
                                        ->setFormTypeOptions([
                                        'multiple'=>true,]),
            ArrayField::new('personnes')->onlyOnDetail(), //afficher la liste sur la page des personnes sur la page détail                          
            ArrayField::new('personnes')->onlyOnIndex(),
            // Fin de gestion de personnes suivant les pages
            // debut de gestion de secteur cible suivant les pages
           AssociationField::new('secteurcibles','Secteur(s) cible')->onlyOnForms()
                                                    ->setFormTypeOptions([
                                                    'multiple'=>true,]),
             ArrayField::new('secteurcibles','Secteur(s) cible')->onlyOnDetail(),/**/ //afficher la liste sur la page des personnes sur la page détail                          
             ArrayField::new('secteurcibles','Secteur(s) cible')->onlyOnIndex(),
            // Fin de gestion de secteur cible suivant les pages

            // debut de gestion de secteur cible suivant les pages
           AssociationField::new('elementsdossi','Elément(s)')->onlyOnForms()
           ->setFormTypeOptions([
           'multiple'=>true,]),
          ArrayField::new('elementsdossi','Elément(s)')->onlyOnDetail(),/**/ //afficher la liste sur la page des personnes sur la page détail                          
          ArrayField::new('elementsdossi','Elément(s)')->onlyOnIndex(),
// Fin de gestion de secteur cible suivant les pages

            TextareaField::new('description','Description')->hideOnIndex(),
            TextareaField::new('rapport','Rapport')->hideOnIndex(),
            //TextField::new('photo'),
            //ImageField::new('piecejoint', 'Photo passport')
              //  ->setBasePath(self::PHOTO_BASE_PATH)
               // ->setUploadDir(self::PHOTO_UPLOAD_DIR)
               // ->setSortable(false),
            BooleanField::new('active'),
            DateTimeField::new('datefermeture', "Date de fermeture")->onlyWhenUpdating(),
            DateTimeField::new('createAt',"crée")->hideOnForm(),
            DateTimeField::new('updateAt',"mise à jour")->hideOnForm()->hideOnIndex(),
         ];
    }
    

    public function persistEntity(EntityManagerInterface $em, $entityInstance): void
    {
        
        if (!$entityInstance instanceof Dossier) {
            return;
        }
        $entityInstance->setCreateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance); 
    }
    public function updateEntity(EntityManagerInterface $em, $entityInstance): void
    {
        
        if (!$entityInstance instanceof Dossier) {
            return;
        }
        $entityInstance->setUpdateAt(new \DateTimeImmutable());
        parent::persistEntity($em, $entityInstance); 
    }
}
