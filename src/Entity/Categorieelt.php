<?php

namespace App\Entity;

use App\Repository\CategorieeltRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CategorieeltRepository::class)]
class Categorieelt
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $createAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updateAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $deleteAt = null;

    #[ORM\Column]
    private ?bool $active = null;

     #[ORM\OneToMany(mappedBy: 'Categorieelt', targetEntity: Elementdossi::class)]
    private Collection $elementdossis;

    public function __construct()
    {
        $this->elementdossis = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeImmutable $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getDeleteAt(): ?\DateTimeImmutable
    {
        return $this->deleteAt;
    }

    public function setDeleteAt(?\DateTimeImmutable $deleteAt): self
    {
        $this->deleteAt = $deleteAt;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection<int, Elementdossi>
     */
    public function getElementdossis(): Collection
    {
        return $this->elementdossis;
    }

    public function addElementdossi(Elementdossi $elementdossi): self
    {
        if (!$this->elementdossis->contains($elementdossi)) {
            $this->elementdossis->add($elementdossi);
            $elementdossi->setCategorieelt($this);
        }

        return $this;
    }

    public function removeElementdossi(Elementdossi $elementdossi): self
    {
        if ($this->elementdossis->removeElement($elementdossi)) {
            // set the owning side to null (unless already changed)
            if ($elementdossi->getCategorieelt() === $this) {
                $elementdossi->setCategorieelt(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->nom;
    }
}
