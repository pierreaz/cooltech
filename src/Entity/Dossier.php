<?php

namespace App\Entity;

use App\Repository\DossierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DossierRepository::class)]
class Dossier
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $nom = null;

    #[ORM\Column(length: 255)]
    private ?string $nature = null;

    #[ORM\Column(length: 255)]
    private ?string $objet = null;

    //#[ORM\Column(type: Types::DATE_MUTABLE)]
    //private ?\DateTimeInterface $dateouverture= null;
    #[ORM\Column]
    private ?\DateTimeImmutable $dateouverture = null;

    //#[ORM\Column(type: Types::DATE_MUTABLE,nullable: true)]
   // private ?\DateTimeInterface $datefermeture = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $datefermeture = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $description = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $rapport = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $fichierjoint = null;

    #[ORM\Column]
    private ?bool $active = null;

    #[ORM\Column(length: 255,nullable: true)]
    private ?string $ouvertpar = null;

    #[ORM\Column(length: 255,nullable: true)]
    private ?string $fermerpar = null;

   


    #[ORM\Column]
    private ?\DateTimeImmutable $createAt = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $updateAt = null;

    #[ORM\ManyToMany(targetEntity: Personne::class, inversedBy: 'dossiers')]
    private Collection $personnes;

    #[ORM\ManyToMany(targetEntity: Secteurcible::class, inversedBy: 'dossiers')]
    private Collection $secteurcibles;

    #[ORM\ManyToMany(targetEntity: Elementdossi::class, inversedBy: 'dossiers')]
    private Collection $elementsdossi;

    public function __construct()
    {
        $this->personnes = new ArrayCollection();
        $this->secteurcibles = new ArrayCollection();
        $this->elementsdossi = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getNature(): ?string
    {
        return $this->nature;
    }

    public function setNature(string $nature): self
    {
        $this->nature = $nature;

        return $this;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    
    public function getDateouverture(): ?\DateTimeImmutable
    {
        return $this->dateouverture;
    }

    public function setDateouverture(\DateTimeImmutable $dateouverture): self
    {
        $this->dateouverture = $dateouverture;

        return $this;
    }
    public function getDatefermeture(): ?\DateTimeImmutable
    {
        return $this->datefermeture;
    }

    public function setDatefermeture(\DateTimeImmutable $datefermeture): self
    {
        $this->datefermeture = $datefermeture;

        return $this;
    }
//
   //

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }
    public function getOuvertpar(): ?string
    {
        return $this->ouvertpar;
    }

    public function setOuvertpar(string $ouvertpar): self
    {
        $this->ouvertpar = $ouvertpar;

        return $this;
    }
    public function getFermerpar(): ?string
    {
        return $this->fermerpar;
    }

    public function setFermerpar(string $fermerpar): self
    {
        $this->fermerpar = $fermerpar;

        return $this;
    }
    //
    public function getRapport(): ?string
    {
        return $this->rapport;
    }

    public function setRapport(string $rapport): self
    {
        $this->rapport = $rapport;

        return $this;
    }

    public function getFichierjoint(): ?string
    {
        return $this->fichierjoint;
    }

    public function setFichierjoint(?string $fichierjoint): self
    {
        $this->fichierjoint = $fichierjoint;

        return $this;
    }

    public function isActive(): ?bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getCreateAt(): ?\DateTimeImmutable
    {
        return $this->createAt;
    }

    public function setCreateAt(\DateTimeImmutable $createAt): self
    {
        $this->createAt = $createAt;

        return $this;
    }

    
    public function getUpdateAt(): ?\DateTimeImmutable
    {
        return $this->updateAt;
    }

    public function setUpdateAt(?\DateTimeImmutable $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return Collection<int, Personne>
     */
    public function getPersonnes(): Collection
    {
        return $this->personnes;
    }

    public function addPersonne(Personne $personne): self
    {
        if (!$this->personnes->contains($personne)) {
            $this->personnes->add($personne);
        }

        return $this;
    }

    public function removePersonne(Personne $personne): self
    {
        $this->personnes->removeElement($personne);

        return $this;
    }

    /**
     * @return Collection<int, Secteurcible>
     */
    public function getSecteurcibles(): Collection
    {
        return $this->secteurcibles;
    }

    public function addSecteurcible(Secteurcible $secteurcible): self
    {
        if (!$this->secteurcibles->contains($secteurcible)) {
            $this->secteurcibles->add($secteurcible);
        }

        return $this;
    }

    public function removeSecteurcible(Secteurcible $secteurcible): self
    {
        $this->secteurcibles->removeElement($secteurcible);

        return $this;
    }

    /**
     * @return Collection<int, Elementdossi>
     */
    public function getElementsdossi(): Collection
    {
        return $this->elementsdossi;
    }

    public function addElementsdossi(Elementdossi $elementsdossi): self
    {
        if (!$this->elementsdossi->contains($elementsdossi)) {
            $this->elementsdossi->add($elementsdossi);
        }

        return $this;
    }

    public function removeElementsdossi(Elementdossi $elementsdossi): self
    {
        $this->elementsdossi->removeElement($elementsdossi);

        return $this;
    }

    
}
